// import { React } from "react";
import { NavLink } from "react-router-dom";
import React, { useState, useEffect } from "react";


function Nav() {
  // const [click, setClick] = useState(false)
  const [open , setOpen] = useState(true)

  useEffect(() => {
    if (!open) {
      document.getElementById("close-button").click()
    }
  },[open]);



  return(
    <>
    <div className="hide-nav">
        <label onClick={()=> setOpen(true)} id="close-button">
        <input type='checkbox' />
        <span className='menu'>
          <span className='hamburger'></span>
        </span>
        <ul className="menu-pad">
          <li>
          <NavLink onClick={() => {document.getElementById("close-button").click()}} className="my-social" to="/">
            Home
            </NavLink>
          </li>
          <li>
          <NavLink onClick={() => {document.getElementById("close-button").click()}} className="my-social" to="about">
            About
            </NavLink>
          </li>
          <li>
          <NavLink onClick={() => {document.getElementById("close-button").click()}} className="my-social" to="portfolio">
            Portfolio
            </NavLink>
          </li>
        </ul>
      </label>
      </div>

      {/* <nav className="navbar fixed-top navbar-expand-lg bg-dark bg-body-tertiary">
        <div className="container-fluid">
        <a class="navbar-brand" href="#">Jesse Morris</a>
          <li className="nav-item">
            <NavLink className="nav-link active px-2" aria-current="page" to="/">
              Home
            </NavLink>
          </li>
        </div>
      </nav> */}
    </>
  )
}

export default Nav;
