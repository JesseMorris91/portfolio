import React from "react";
import moviemixer1 from "../media/moviemixer1.png";
import dealertrax from "../media/DealerTrax.png";

function Portfolio() {



  return(

    <>



      <div className="container1">
        <h1 id="portfolio-title">Portfolio</h1>
        <div className="container text-center">
          <div className="row">
            <div className="col pb-5">
              <div className="movie-wrapper">
                <h3>MovieMixer</h3>
                  <p>MovieMixier is an app where you can search for thousands of movies,
                    add them into your list of favorite movies and order them from best to worst.
                    You can also see other users movie lists to find a new movie to watch.
                    We even included a Movie Trivia game to help pass the time.</p>
                  <button type="button" class="btn btn-outline-secondary bg-transparent px-3 m-3"><i class="fa-brands fa-gitlab"></i> Repository</button>
                  <button type="button" class="btn btn-outline-light bg-transparent px-3">Learn More</button>
              </div>
            </div>
            <div className="col pt-5">
              <img id="project1" src={moviemixer1} />
            </div>
          </div>
          <div className="row">
            <div className="col pb-5">
              <div className="movie-wrapper">
                <h3>DealerTraX</h3>
                  <p>DealerTraX - Car dealership admin for employees, to track sales history, sales appointments,
                   service history, service appointments, can add new employees/technicians with employee id.
                    Can look up cars in inventory by VIN, As well as track an individual Sales Reps sales history by employee id number.
                     With a customer UI Home Page.</p>
                  <button type="button" class="btn btn-outline-secondary bg-transparent px-3 m-3"><i class="fa-brands fa-gitlab"></i> Repository</button>
                  <button type="button" class="btn btn-outline-light bg-transparent px-3">Learn More</button>
              </div>
            </div>
            <div className="col pt-5">
              <img id="project1" src={dealertrax} />
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default Portfolio;
