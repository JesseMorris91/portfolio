import React from "react";
import { useNavigate } from "react-router-dom";
import { useTypewriter, Cursor } from 'react-simple-typewriter';
import '../index.scss'
import jesse2 from "../media/jesse2.png"

function MainPage() {
  const navigate = useNavigate();

  const [text] = useTypewriter({
    words: ['Full-Stack Developer', 'UX UI Designer', 'Coding Enthusiast', 'Problem Solver', 'Solutions Expert'],
    loop: false,
    typeSpeed: 150,
    deleteSpeed: 80,
  });

  const [textName] = useTypewriter({
    words: ['Hello, my name is Jesse Morris.'],
    loop: 1,
    typeSpeed: 40,
    deleteSpeed: 80,
  });


  function redirectToPortfolio(event) {
    event.preventDefault();
    window.scroll(0, 0);
    return navigate("/portfolio");
  }


  return (
    <>
        <div className="page-bg">
          <div className="animation-wrapper">
            <div className="particle particle-1"></div>
            <div className="particle particle-2"></div>
            <div className="particle particle-3"></div>
            <div className="particle particle-4"></div>
          </div>

          {/* <div className="page-wrapper"> */}
            {/* <h1>CSS Particles</h1> */}
          </div>

            <div className="container-fluid">
              <div className="jesse">
                <div className="jesse-wrapper">
                  <h2 className="jesse-center">
                    <div className="jesse-text">
                        {/* Hello, my name is Jesse Morris. */}
                        <span  className="">
                          {textName}
                        </span>
                      <section className="ima">
                        I'm a {' '}
                        <span className="hobbies">
                          {text}
                        </span>
                        <span className="cursor-style">
                          <Cursor cursorStyle='|' />
                        </span>
                      </section>
                    </div>
                  </h2>
                </div>
                <div className="profile-image-container">
                  <img className="profile-image" src={jesse2} alt=""/>
                </div>

                <div className="tech-stack-wrap">
                {/* <div className="profile-image-container">
                  <img className="profile-image" src={jesse2} />
                </div> */}
                  <h2 className="my-tech-stack">My Tech Stack</h2>
                  <div className="tech-box">
                    <button type="button" className="btn btn-outline-light disabled m-2"><i className="fa-brands fa-python"></i> Python</button>
                    <button type="button" className="btn btn-outline-light disabled m-2"><i className="fa-brands fa-js"></i> JavaScript</button>
                    <button type="button" className="btn btn-outline-light disabled m-2"><i className="fa-brands fa-react"></i> React</button>
                    <button type="button" className="btn btn-outline-light disabled m-2"><i className="fa-brands fa-python"></i> Django</button>
                    <button type="button" className="btn btn-outline-light disabled m-2"><i className="fa-solid fa-bolt"></i> FastApi</button>
                    <button type="button" className="btn btn-outline-light disabled m-2"><i className="fa-brands fa-docker"></i> Docker</button>
                    <button type="button" className="btn btn-outline-light disabled m-2"><i className="fa-brands fa-gitlab"></i> GitLab</button>
                    <button type="button" className="btn btn-outline-light disabled m-2"><i className="fa-brands fa-html5"></i> HTML</button>
                    <button type="button" className="btn btn-outline-light disabled m-2"><i className="fa-brands fa-css3-alt"></i> CSS</button>
                    <button type="button" className="btn btn-outline-light disabled m-2"><i className="fa-solid fa-database"></i> PostgreSQL</button>
                    <button type="button" className="btn btn-outline-light disabled m-2"><i className="fa-solid fa-cloud"></i> REST APIs</button>
                    <button type="button" className="btn btn-outline-light disabled m-2"><i className="fa-brands fa-windows"></i> Powershell</button>
                    <button type="button" className="btn btn-outline-light disabled m-2"><i className="fa-brands fa-sass"></i> SCSS</button>
                    <button type="button" className="btn btn-outline-light disabled m-2"><i className="fa-solid fa-database"></i> SQlite3</button>
                    <button type="button" className="btn btn-outline-light disabled m-2"><i className="fa-brands fa-wordpress"></i> Wordpress</button>
                </div>
              </div>
                <div className="button">
                  <a href="https://gitlab.com/JesseMorris91"><i className="fa-brands fa-square-gitlab fa-2x"></i></a>
                  <a href="https://www.linkedin.com/in/jessem91/"><i className="fa fa-linkedin fa-2x"></i></a>
                  <a href="https://www.youtube.com/channel/UCCe-cUxLOAEDqFX6Ipohk4w"><i className="fa fa-youtube fa-2x"></i></a>
                  <a href="portfolio" onClick={redirectToPortfolio}><i className="fa-regular fa-folder-open fa-2x"></i></a>
                </div>
              </div>
        </div>
        {/* </div> */}
        {/* <div className="profile-pic">
        <p>Hello</p>
          </div> */}
      </>

  );
}

export default MainPage;
