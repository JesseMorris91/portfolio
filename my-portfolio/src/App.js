import './App.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import MainPage from './components/MainPage';
import Portfolio from './components/Portfolio';
import Nav from './Nav';
import Space from './components/Space';
import About from './components/About';

function App() {
  return (
    <BrowserRouter>
      <Nav />
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="portfolio" element={<Portfolio />} />
          <Route path="space" element={<Space />} />
          <Route path="about" element={<About />} />
        </Routes>
    </BrowserRouter>
  );
}

export default App;
